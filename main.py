from __future__ import print_function
import sys
import matplotlib.pyplot as plt
import numpy as np
import numpy.linalg as li
from collections import defaultdict
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Line3DCollection

np.set_printoptions(precision=2, threshold=200, edgeitems=6, linewidth=200, suppress=True)

# Print current step
PROGRESS = True

# Debug matrices
DEBUG = True

# Trace calculation of matrices
TRACE = False

# Use sparse matrices when possible
SPARSE = True


# Setup so we can handle sparse and dense in the same way
if SPARSE:
	import scipy.sparse as sp
	import scipy.sparse.linalg as spli
	matrix = sp.lil_matrix
	solve = spli.spsolve
	def dprint(*e, **kwargs):
		if DEBUG:
			print(*[x.A if sp.issparse(x) else x for x in e], **kwargs)
else:
	matrix = np.zeros
	solve = li.solve
	def dprint(*e, **kwargs):
		if DEBUG:
			print(*e, **kwargs)

# Debug functions
def ddprint(*e, **kwargs):
	if TRACE:
		dprint(*e, **kwargs)
def pprint(*args, **kwargs):
	if PROGRESS:
		print(*args, **kwargs)


# Utility functions
def parse_vec(line, n=3):
	return [float(x) for x in line.split()[:n]]
def parse_ivec(line, n=3):
	return [int(x) for x in line.split()[:n]]
def angle(vec1, vec2):
	cosine = np.dot(vec1, vec2) / li.norm(vec1) / li.norm(vec2)
	return np.arccos(np.clip(cosine, -1, 1))
def transform(matrix, vector):
	if len(vector) == 3 and matrix.shape == (4, 4):
		vector = np.append(vector, 1)
	return matrix.dot(vector.T)[:3]


def strlim(strn):
	return strn if len(strn) < 100 else strn[:96] + '...' + strn[-1]


class Deformer:

	@property
	def center(self):
		return np.mean(self.transformed_vertices[:self.num_vertices], axis=0)
	
	@property
	def size(self):
		return np.ptp(self.transformed_vertices[:self.num_vertices], axis=0)

	def __init__(self, filename):
		self.energy = 0
		pprint('Loading %s' % filename)
		self.num_vertices, self.num_faces, num_edges = 0, 0, 0
		self.selected = set()
		self.anchored = set()
		self.transformation = np.identity(4)

		with open(filename) as f:
			lines = (line.rstrip() for line in f if line.rstrip())

			if next(lines) != 'OFF':
				raise ValueError('Expected OFF header')
			self.num_vertices, self.num_faces, self.num_edges = parse_ivec(f.readline())
			if self.num_edges:
				raise ValueError('Edges unsupported')

			pprint('Loading vertices')
			self.vertices = np.zeros((self.num_vertices, 3))
			for row in range(self.num_vertices):
				self.vertices[row] = parse_vec(next(lines))
			dprint(self.vertices)
			dprint()

			self.faces = []
			for _ in range(self.num_faces):
				n, v1, v2, v3 = parse_ivec(next(lines), n=4)
				self.faces.append((v1, v2, v3))

		self.transformed_vertices = self.vertices.copy()
		self.fixed_vertices = {}

		pprint('Computing vertex adjacency')
		self.vertex_adjacency = matrix((self.num_vertices, self.num_vertices), dtype=np.int)
		self.num_adjacent = matrix((self.num_vertices, self.num_vertices), dtype=np.int)
		self.laplacian = matrix((self.num_vertices, self.num_vertices), dtype=np.int)

		self.adjacent_edge = defaultdict(set)
		self.adjacent_edges = defaultdict(set)
		for v1, v2, v3 in self.faces:
			self.vertex_adjacency[v1, v2] = 1
			self.vertex_adjacency[v2, v1] = 1
			self.vertex_adjacency[v2, v3] = 1
			self.vertex_adjacency[v3, v2] = 1
			self.vertex_adjacency[v3, v1] = 1
			self.vertex_adjacency[v1, v3] = 1

			self.adjacent_edge[(v1, v2)].add(v3)
			self.adjacent_edge[(v2, v3)].add(v1)
			self.adjacent_edge[(v1, v3)].add(v2)
			self.adjacent_edge[(v3, v1)].add(v2)
			self.adjacent_edge[(v2, v1)].add(v3)
			self.adjacent_edge[(v3, v2)].add(v1)

			self.adjacent_edges[v1].add(v2)
			self.adjacent_edges[v1].add(v3)
			self.adjacent_edges[v2].add(v1)
			self.adjacent_edges[v2].add(v3)
			self.adjacent_edges[v3].add(v1)
			self.adjacent_edges[v3].add(v2)
		dprint(self.vertex_adjacency)
		dprint()

		pprint('Counting adjacent edges')
		for row in range(self.num_vertices):
			self.num_adjacent[row, row] = len(self.adjacent_edges[row])
		dprint(self.num_adjacent)
		dprint()

		self.compute_weights()

		pprint('Computing laplacian matrix')
		self.laplacian = self.weight_sum - self.weights
		dprint(self.laplacian)
		dprint()

		self.precompute_Pi_Di()

	def compute_weights(self):
		pprint('Computing weight matrix')
		self.weights = matrix((self.num_vertices, self.num_vertices), dtype=np.float)
		self.weight_sum = matrix((self.num_vertices, self.num_vertices), dtype=np.float)
		for vi_i in range(self.num_vertices):
			vi = self.vertices[vi_i]
			adjacent = self.adjacent_edges[vi_i]
			ddprint(vi_i, 'has', adjacent)
			for vj_i in adjacent:
				ddprint(vi_i, '->', vj_i, end='')
				vj = self.vertices[vj_i]
				weight = 0.0
				for other_i in self.adjacent_edge[(vi_i, vj_i)]:
					other = self.vertices[other_i]
					weight += 1.0 / np.tan(angle(vi - other, vj - other))
					ddprint('\t', vi_i, other_i, vj_i, 'angle is', int(angle(vj - other, vi - other) * 180 / np.pi))
				self.weights[vi_i, vj_i] = weight / 2.0
				self.weight_sum[vj_i, vj_i] += weight / 2.0
			ddprint()
		dprint(self.weights)
		dprint()
		pprint('Computing weight sum')
		dprint(self.weight_sum)
		dprint()

	def precompute_Pi_Di(self):
		pprint('Precomputing P and D')
		self.Pi_sub_Pj = []
		self.D = []
		for vi_i in range(self.num_vertices):
			num_neighbours = self.num_adjacent[vi_i, vi_i]
			P_i = np.zeros((3, num_neighbours))
			D_i = np.zeros((num_neighbours, num_neighbours))
			for i, vj_i in enumerate(sorted(self.adjacent_edges[vi_i])):
				P_i[:, i] = self.vertices[vi_i] - self.vertices[vj_i]
				D_i[i, i] = self.weights[vi_i, vj_i]
			self.Pi_sub_Pj.append(P_i)
			self.D.append(D_i)

	def select(self, filename):
		pprint('Selecting vertices from %s' % filename)
		with open(filename) as f:
			row = 0
			if f.readline() == '!slice\n':
				for line in f.readlines():
					mode, selector, coord, value = line.split()
					coord = {'x': 0, 'y': 1, 'z': 2}[coord]
					selected = set()
					if selector == 'above':
						selected, = np.where(self.vertices[:, coord] > float(value))
					elif selector == 'below':
						selected, = np.where(self.vertices[:, coord] < float(value))

					pprint(mode, selected)
					if mode == 'select':
						self.selected = self.selected.union(list(selected))
					elif mode == 'anchor':
						self.anchored = self.anchored.union(list(selected))

			else:
				f.seek(0)
				for line in f.readlines():
					if line.startswith('#'):
						continue
					line = int(line)
					if line == 0:
						self.anchored.add(row)
					elif line == 2:
						self.selected.add(row)
					row += 1

		dprint('Anchored:', strlim(str(self.anchored)))
		dprint('Selected:', strlim(str(self.selected)))
		dprint()

	def transform(self, filename, precomputed_intial=None):
		pprint('Loading transformation')
		with open(filename) as f:
			row = 0
			for line in f.readlines():
				if line.startswith('#'):
					continue
				self.transformation[row] = parse_vec(line, n=4)
				row += 1
		dprint(self.transformation)
		dprint()
		pprint('Transforming vertices')
		for selected_i in self.selected:
			vi_new = transform(self.transformation, self.vertices[selected_i])
			self.fixed_vertices[selected_i] = vi_new
		for anchored_i in self.anchored:
			self.fixed_vertices[anchored_i] = self.vertices[anchored_i]

		rows = self.num_vertices + len(self.fixed_vertices)
		self.A = matrix((rows, rows))
		self.b = np.zeros((rows, 3))

		pprint('Calculating A')
		self.A[:self.num_vertices, :self.num_vertices] = self.laplacian
		row = self.num_vertices
		for i in self.fixed_vertices:
			self.A[row, i] = 1
			self.A[i, row] = 1
			self.b[row] = self.fixed_vertices[i]
			row += 1
		dprint(self.A)
		dprint()
		if SPARSE:
			pprint('Factorizing A')
			self.solver = spli.factorized(self.A)
			dprint(self.solver)

		if precomputed_intial:
			pprint('Loading P` from "%s"' % precomputed_intial)
			with open(precomputed_intial) as f:
				lines = (line.rstrip() for line in f if line.rstrip())
				if next(lines) != 'OFF':
					raise ValueError('Expected OFF header')
				num_vertices, num_faces, num_edges = parse_ivec(f.readline())
				assert self.num_vertices == num_vertices and self.num_faces == num_faces

				for row in range(self.num_vertices):
					self.transformed_vertices[row] = parse_vec(next(lines))
				dprint(self.transformed_vertices)

		else:
			pprint('Calculating initial P`')
			self.transformed_vertices = self.solve_vertices(None)

	def solve_vertices(self, rotations):
		pprint('Calculating b')
		self.energy = 0
		if rotations:
			for vi_i in range(self.num_vertices):
				self.b[vi_i] = 0
				ddprint('b_%d = [0, 0, 0]' % vi_i)
				for x, vj_i in enumerate(sorted(self.adjacent_edges[vi_i])):
					self.b[vi_i] += (self.weights[vi_i, vj_i] / 2) * (rotations[vi_i] + rotations[vj_i]).dot(self.Pi_sub_Pj[vi_i][:, x])
					self.energy += self.weights[vi_i, vj_i] * li.norm((self.transformed_vertices[vi_i] - self.transformed_vertices[vj_i]) - rotations[vi_i].dot(self.Pi_sub_Pj[vi_i][:, x]))
				ddprint('b_%d =' % vi_i, self.b[vi_i])
				ddprint()
		else:
			self.b[:self.num_vertices] = self.laplacian * self.vertices
		dprint(self.b)
		pprint('Solving P`')
		if SPARSE:
			transformed_vertices = self.solver(self.b)
		else:
			transformed_vertices = solve(self.A, self.b)
		dprint(transformed_vertices)
		dprint()

		return transformed_vertices[:self.num_vertices]

	def deform(self):
		pprint('Calculating rotations')
		rotations = []

		for vi_i in range(self.num_vertices):
			num_neighbours = self.num_adjacent[vi_i, vi_i]
			P_i_prime = np.zeros((num_neighbours, 3))
			for x, vj_i in enumerate(sorted(self.adjacent_edges[vi_i])):
				P_i_prime[x] = self.transformed_vertices[vi_i] - self.transformed_vertices[vj_i]

			S_i = self.Pi_sub_Pj[vi_i].dot(self.D[vi_i]).dot(P_i_prime)

			U, s, V_t = li.svd(S_i)
			rotation = V_t.T.dot(U.T)
			if li.det(rotation) < 0:
				U[:,len(s) - 1] *= -1
				rotation = V_t.T.dot(U.T)
				assert li.det(rotation) > 0

			rotations.append(rotation)

		dprint(str(rotations[:3]).replace('\n', ' '), '...')
		self.transformed_vertices = self.solve_vertices(rotations)

		self.compute_weights()
		self.laplacian = self.weight_sum - self.weights
		self.precompute_Pi_Di()

	def save(self, filename):
		with open(filename, 'w') as f:
			f.write('OFF\n')
			f.write('%d %d 0\n' % (self.num_vertices, self.num_faces))
			for row in range(self.num_vertices):
				f.write('%f %f %f\n' % tuple(self.transformed_vertices[row].T))
			for face in self.faces:
				f.write('3 %d %d %d\n' % face)

	def display(self, color='b', show_original=False):
		fig = plt.figure()
		ax = Axes3D(fig)

		original = []
		transformed = []
		def vert(v):
			return (v[0], v[2], v[1])
		for face in range(self.num_faces):
			transformed.append([
				vert(self.transformed_vertices[self.faces[face][0]]),
				vert(self.transformed_vertices[self.faces[face][1]])
			])
			transformed.append([
				vert(self.transformed_vertices[self.faces[face][1]]),
				vert(self.transformed_vertices[self.faces[face][2]])
			])
			transformed.append([
				vert(self.transformed_vertices[self.faces[face][2]]),
				vert(self.transformed_vertices[self.faces[face][0]])
			])
			original.append([
				vert(self.vertices[self.faces[face][0]]),
				vert(self.vertices[self.faces[face][1]])
			])
			original.append([
				vert(self.vertices[self.faces[face][1]]),
				vert(self.vertices[self.faces[face][2]])
			])
			original.append([
				vert(self.vertices[self.faces[face][2]]),
				vert(self.vertices[self.faces[face][0]])
			])

		ax.add_collection(Line3DCollection(transformed, color=color))
		if show_original:
			ax.add_collection(Line3DCollection(original, color='grey'))
		for vss, c in [(self.selected, 'r'), (self.anchored, 'b')]:
			if vss:
				pts = np.take(self.transformed_vertices, list(vss), axis=0)
				ax.scatter(pts[:,0], pts[:,2], pts[:,1], c=c, s=100)

		low = np.min(self.transformed_vertices, axis=0)
		ranges = np.max(self.transformed_vertices, axis=0) - low
		max_range = max(ranges)
		ax.set_xlim(low[0] + (ranges[0] - max_range)/2, low[0] + max_range)
		ax.set_ylim(low[2] + (ranges[2] - max_range)/2, low[2] + max_range)
		ax.set_zlim(low[1] + (ranges[1] - max_range)/2, low[1] + max_range)
		plt.draw()

if __name__ == '__main__':
	deformer = Deformer(sys.argv[1])
	deformer.select(sys.argv[2])

	deformer.transform(sys.argv[3], sys.argv[4] if len(sys.argv) > 4 else None)
	deformer.display(show_original=True)
	deformer.save('out0.off')

	deformer.deform()
	# deformer.display()
	# deformer.save('out1.off')

	DEBUG = PROGRESS = False

	last = deformer.energy
	for i in range(100):
		deformer.deform()
		print(i+1, '\t', 100)
		print(deformer.energy)
		if deformer.energy > last - 0.5:
			print('Stopping deformation - energy not decreasing')
			break
		last = deformer.energy
	deformer.display()
	deformer.save('out100.off')

	#plt.show()
